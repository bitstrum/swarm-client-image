#!/bin/sh
set -e

case $(uname -m) in
  "armv7l")
    DOCKER_ARCH="armhf"
    ;;
  "aarch64")
    DOCKER_ARCH="aarch64"
    ;;
  "x86_64")
    DOCKER_ARCH="x86_64"
    ;;
esac

# Install docker client via binaries
wget -q https://download.docker.com/linux/static/stable/${DOCKER_ARCH}/docker-${DOCKER_VERSION}.tgz
tar xzf docker-${DOCKER_VERSION}.tgz
mv docker/docker /usr/local/bin/docker
chmod +x /usr/local/bin/docker
rm -rf docker*
