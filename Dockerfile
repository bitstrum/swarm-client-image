FROM debian:9

ENV DOCKER_VERSION "19.03.5"
ENV SWARM_CLIENT_VERSION "3.17"

WORKDIR /opt/swarm-client

COPY install-docker.sh .
COPY install-swarm-client.sh .
COPY config.json /root/.docker/
RUN apt-get update \
 && apt-get install --no-install-recommends --yes \
    git \
    openjdk-8-jre-headless \
    ssh \
    wget \
 && apt-get clean && rm -rf /var/lib/apt/lists/* \
 && sh install-swarm-client.sh \
 && sh install-docker.sh

COPY swarm-client.sh .
RUN chmod +x swarm-client.sh

CMD ["./swarm-client.sh"]
