#!/bin/sh
# Exit on error
set -e

option() {
    [ "${2}" != "" ] && CLIENT_OPTS="${CLIENT_OPTS} ${1} ${2}"
}

readonly CLIENT="swarm-client.jar"
CLIENT_OPTS="\
  -master ${JENKINS_URL_PROTOCOL}://${JENKINS_URL}:${JENKINS_HTTP_PORT}/ \
  -mode exclusive \
  -disableClientsUniqueId \
"

option "-name" "${NAME}"
option "-executors" "${EXECUTORS}"
option "-username" "${USERNAME}"
option "-password" "${PASSWORD}"

# Option tunnel
TUNNEL_OPT=":"
if [ "${JENKINS_JNLP_URL}" != "" ]; then
  TUNNEL_OPT="${JENKINS_JNLP_URL}${TUNNEL_OPT}"
fi
if [ "${JENKINS_JNLP_PORT}" != "" ]; then
  TUNNEL_OPT="${TUNNEL_OPT}${JENKINS_JNLP_PORT}"
fi
if [ "${TUNNEL_OPT}" != ":" ]; then
  CLIENT_OPTS="${CLIENT_OPTS} -tunnel ${TUNNEL_OPT}"
fi

# Option labels
for label in ${LABELS}; do
  CLIENT_OPTS="${CLIENT_OPTS} -labels '${label}'"
done

java -jar ${CLIENT} ${CLIENT_OPTS}
