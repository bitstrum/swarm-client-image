#!/bin/sh

IMAGE_NAME="swarm-client"
HOSTNAME="$(hostname)"

docker rm -f ${IMAGE_NAME} 2>/dev/null || true
docker system prune -af
docker build -t ${IMAGE_NAME} .
docker run \
  -d \
  --name ${IMAGE_NAME} \
  --restart unless-stopped \
  -h ctn-${HOSTNAME} \
  --env NAME=swarm-client-${HOSTNAME} \
  --env-file ./swarm-client.properties \
  -v /var/run/docker.sock:/var/run/docker.sock \
  swarm-client
